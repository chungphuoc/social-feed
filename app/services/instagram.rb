class Instagram
  attr_accessor :url, :query
  def initialize(query)
    @query = query
    @url = "https://www.instagram.com/explore/tags/#{query}"
  end

  def extract_data
    tries = 2
    begin
      response = HTTParty.get(URI.escape(@url))
    rescue Errno::ECONNREFUSED => e
      (tries -= 1) > 0 ? retry : nil
    rescue Zlib::BufError => e
      (tries -= 1) > 0 ? retry : nil
    end

    data = response.body[%r{<script type="text/javascript">window\._sharedData = (.*)</script>}, 1] if response.present?

    if data.present? && data.include?("\;")
      data.delete!(';')
      JSON.parse(data)['entry_data']['TagPage'].first['graphql']['hashtag']
    else
      puts "Something went wrong"
      return nil
    end
  end
end
