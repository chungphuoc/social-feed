class HomesController < ApplicationController
  def index
    result = Instagram.new(params[:hashtag].presence || "phil").extract_data

    data = result['edge_hashtag_to_media']["edges"].present? ? result['edge_hashtag_to_media']["edges"] : []

    @data = Kaminari.paginate_array(data).page(params[:page]).per(10)
  end
end
