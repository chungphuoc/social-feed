# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:
## System dependencies

* Ruby version
  `2.6.3`

* Rails version
  `5.2.3'`

* System dependencies
 `Postgresql`
 `Rails`

* Setup local env
  `cp .env.example .env`
  `cp config/database.yml.sample config/database.yml`

* Database creation
  `rails db:create`
  `rails db:migrate`

* Database initialization
  `rails db:seed`

* How to run the test suite
  `bundle exec rspec spec`

