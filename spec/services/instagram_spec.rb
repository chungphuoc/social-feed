require 'rails_helper'

RSpec.describe Instagram do
  let(:instagram_data) { JSON.parse(IO.read 'spec/fixtures/instagram.user_recent_media.json') }
  let!(:service) { Instagram.new('cr7') }

  def extract_data(service)
    service.extract_data
  end

  describe '#extract_data' do
    subject { extract_data(service) }

    context 'success get data instagram' do
      it { is_expected.to_not be_nil }
    end

    context 'empty data' do
      before { allow(HTTParty).to receive(:get).and_return(nil) }
      it { is_expected.to eq nil }
    end

    context 'Errno::ECONNREFUSED' do
      before { allow(HTTParty).to receive(:get).and_raise(Errno::ECONNREFUSED) }
      it { is_expected.to eq nil }
    end

    context 'Zlib::BufError' do
      before { allow(HTTParty).to receive(:get).and_raise(Zlib::BufError) }
      it { is_expected.to eq nil }
    end
  end
end
